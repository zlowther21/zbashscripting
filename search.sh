#!/bin/bash

: << 'license'
The MIT License (MIT)

Copyright (c) 2014 Zachary Lowther

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

license


main(){
	if [ "$#" -eq 1 ] && [ $1 == "--help" ]; then
		displayHelp 
	elif [ "$#" -lt 2 ]; then
		echo "
Error: Function Misused - Consult Help Page (./search.sh --help)

Explanation: The 'search' command requires at least 2 parameters to work correctly. Please review the Help section by
typing './search.sh --help' to learn the correct usage of the 'search' command."
	else
		if [ "$#" -ne 3 ]; then
			search $1 $2
		else
			search $1 $2 $3
		fi
	fi
	
}

function search {
	
	if [ -z $3 ]; then
		find $1 -type f -name "*.$2"
	else
		echo
		echo "The Following Files contain the string: $3"
		find $1 -type f -name "*.$2" -print0 | xargs -0 grep -il $3
		echo
	fi

}


function displayHelp() {
	echo "
Hi and Welcome to the Search Command Help Page.

The 'search' command requires 2 parameters:
    * Parameter 1 -> The Drive or Directory In which you wish to search for the file, for example '/home'
    * Parameter 2 -> The Extension of the file you are searching for, for example 'txt'
    * An optional third parameter allows you to search for a given string found in files returned by
      searching the given drive and the given file extension. The program will iterate over the files
      found and return only files that contain the given string.

    ***Example Usage***
    - Normal Usage: ./search.sh /home/myuser txt
    - With Third Parameter to search for a string: ./search.sh /home/myuser txt samplestring


    If you have further questions regarding the usage of bash, please visit http://www.gnu.org/software/bash/
"
}

main "$@"


