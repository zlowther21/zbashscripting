#!/bin/bash

: << 'license'
The MIT License (MIT)

Copyright (c) 2014 Zachary Lowther

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

license



main() {
	if [ "$#" -eq 1 ] && [ $1 == "--help" ]; then
		displayHelp 
	elif [ "$#" -lt 2 ]; then
		echo "
Error: Function Misused - Consult Help Page (./get.sh --help)

Explanation: The 'get' command requires at least 2 parameters to work correctly. Please review the Help section by
typing './get.sh --help' to learn the correct usage of the 'get' command."
	else
		curl -L "$2/$1"
		if [ -z $3 ]; then
			#No Third parameter
			echo
		else
			if [ -n $3 ] && [ "$3" == "--browser" ]; then
				webaddress=$2
				if [[ $2 != *http://* && $2 != *https://* ]]; then
					webaddress="http://$2"
				fi
				echo $webaddress
				python -mwebbrowser "$webaddress/$1"
			elif [ -n $3 ] && [ "$3" != "--browser" ]; then
				echo
				echo "Error: The third parameter is invalid. Try --browser instead."
				echo
			fi
		fi
		
	fi
}

function displayHelp() {
	echo "
Hi and Welcome to the Get Command Help Page.

The 'get' command requires 2 parameters:
    * Parameter 1 -> HTML Page you wish to load from a given web server, for example 'index.html'
    * Parameter 2 -> Web Address of the server to which you wish to make a request, for example 'http://www.byu.edu'
        -- Special Note -> You do not need to worry about adding 'http' or 'www' to the web address if you do not want to do so
    * An optional third parameter allows you to open a web browser and show the result of your search. Use the '--browser' flag.

    ***Example Usage***
    Normal Usage: ./get.sh index.html byu.edu
    With Third Parameter to Open Web Browser: ./get.sh index.html byu.edu --browser


    If you have further questions regarding the usage of bash, please visit http://www.gnu.org/software/bash/
"
}



main "$@"