#!/bin/bash

: << 'license'
The MIT License (MIT)

Copyright (c) 2014 Zachary Lowther

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

license

main() {
	ip_info
	dns_info
}

function displayHelp() {
	echo "
Hi and Welcome to the Get Command Help Page.

The 'get' command requires 2 parameters:
    * Parameter 1 -> HTML Page you wish to load from a given web server, for example 'index.html'
    * Parameter 2 -> Web Address of the server to which you wish to make a request, for example 'http://www.byu.edu'
        -- Special Note -> You do not need to worry about adding 'http' or 'www' to the web address if you do not want to do so
    * An optional third parameter allows you to open a web browser and show the result of your search. Use the '--browser' flag.

    ***Example Usage***
    Normal Usage: ./get.sh index.html byu.edu
    With Third Parameter to Open Web Browser: ./get.sh index.html byu.edu --browser


    If you have further questions regarding the usage of bash, please visit http://www.gnu.org/software/bash/
"
}

function dns_info(){
	echo
	for (( i = 0; i < 17; i++ )); do
		if [ $i -lt 8 ] || [ $i -gt 8 ]; then
			printf "-"
		elif [ $i -eq 8 ]; then
			printf "System DNS Servers"
		fi
	done
	printf "\n"

	cat /etc/resolv.conf | sed -n '/^nameserver/p'
}

function ip_info() {
	echo 
	for (( i = 0; i < 17; i++ )); do
		if [ $i -lt 8 ] || [ $i -gt 8 ]; then
			printf "-"
		elif [ $i -eq 8 ]; then
			printf "Network Interfaces and Associated IP Addresses"
		fi
	done
	printf "\n"
	#netstat -i
	route -n

	echo

	ifconfig

	echo 

	for (( i = 0; i < 17; i++ )); do
		if [ $i -lt 8 ] || [ $i -gt 8 ]; then
			printf "-"
		elif [ $i -eq 8 ]; then
			printf "Public IP Address"
			#Uses the curl command contacting a webservice which returns a public ip address
		fi
	done

	printf "\n"
	curl icanhazip.com

	echo
}


main "$@"